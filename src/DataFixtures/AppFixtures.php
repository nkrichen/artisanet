<?php

namespace App\DataFixtures;

use App\Entity\Commande;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 20; $i++) {
            $commande = new Commande();
            $commande->setLibelle('commande- '.$i);
            $manager->persist($commande);
        }

        $manager->flush();
    }
}
