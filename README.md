# Artisanet

## Install project
```
composer install
```
## Database update

## Database create
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
```
## Database update
```
php bin/console doctrine:schema:update [--force | --dump-sql]
```
## Load fixtures : Commande, Client ...
```
php bin/console d:f:l
```

## How install new package (exple fixture-bundle)
```
php bin/console d:f:l
```



